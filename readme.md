**How to run**

1. Install docker and docker compose on your computer.
2. Enter project root
3. Run "docker-compose up -d"
4. Enter container "docker exec -it dojodockerphp_php71_1 bash"
5. Run "composer install" on container
6. Write your code in code/src and tests in code/test
7. Run "vendor/bin/phpunit"